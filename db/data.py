import datetime
from flask_login import LoginManager, UserMixin
from sqlalchemy import create_engine, Column, ForeignKey, Integer, String, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base

URL_DB = 'sqlite:///chats.db'

Base = declarative_base()

class Chat(Base):
    __tablename__ = 'chat'
    id = Column(Integer, primary_key = True)
    name = Column(String(100), unique = True)

    def __init__(self, name):
        self.name = name

class User(UserMixin,Base):
    __tablename__ = 'user'
    id = Column(Integer, primary_key = True)
    username = Column(String(40), unique = True)
    password = Column(String(80))
    admin = Column(String(1))

class Message(Base):
    __tablename__ = 'message'
    id = Column(Integer, primary_key = True)
    msg = Column(Text)
    user_id = Column(Integer, ForeignKey('user.id'))
    chat_id = Column(Integer, ForeignKey('chat.id'))
    time = Column(DateTime, default=datetime.datetime.utcnow)

    def __init__(self, msg, user_id, chat_id):
        self.msg = msg
        self.user_id = user_id
        self.chat_id = chat_id

engine = create_engine(URL_DB, connect_args={'check_same_thread': False})

Base.metadata.create_all(engine)