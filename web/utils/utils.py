import csv
import re
import requests

def get_quote_per_share(msg):
    message = ""
    stock_code = msg.split('/stock=')[1]
    try:
        with requests.Session() as s:
            csv_url = 'https://stooq.com/q/l/?s=%s&f=sd2t2ohlcv&h&e=csv' % stock_code
            download = s.get(csv_url)
            decoded_content = download.content.decode('utf-8')
            cr = csv.reader(decoded_content.splitlines(), delimiter=',')
            stock = list(cr)[1][3]

        if 'N/D' not in str(stock):
            message = "%s quote is $%s per share" % (stock_code, str(stock))
        else:
            message = 'Sorry, couldn\'t find the quote. Please try again'
    except:
        message = 'Sorry, something went wrong. Please try again later'
    return message