# Local imports
from db.data import User, Chat, Message, engine
from utils.utils import *

# server imports
from flask import Flask, render_template, request, redirect, url_for, g
from flask_login import LoginManager, UserMixin, login_user, login_required, current_user, logout_user
from flask_socketio import SocketIO, send

# ORM imports
from sqlalchemy.orm import sessionmaker

# OS imports
import os

app = Flask(__name__)
app.config['SECRET_KEY'] = '\x16+kZ\xde\x9d\x00\xe6j\xf7E\xd3S\xa1\x8f\x11\x7f!\x90\x8a\xb3\xe9|\x8b'

socketio = SocketIO(app)

# We use the engine from data file
DBSession = sessionmaker(bind=engine)
session = DBSession()

login_manager = LoginManager()
login_manager.init_app(app)

@login_manager.user_loader
def load_user(user_id):
    return session.query(User).get(int(user_id))

@app.route('/', methods=["GET","POST"])
def index():
    if request.method == 'GET':
        if current_user.is_authenticated:
            return render_template('index.html')
        else:
            return render_template('login.html')
    else:
        user = session.query(User).filter_by(username=request.form['username'], password=request.form['password']).first()
        if user:
            login_user(user)
            return render_template('index.html')
        else:
            return render_template('login.html')

@app.before_request
def before_request():
    g.user = current_user
    g.chat_channels = get_chats()

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

def get_chats():
    chats = session.query(Chat).all()
    return  chats

@app.context_processor
def inject_user():
    return dict(user=g.user, chats=g.chat_channels)

@app.route('/show_chat/<int:id>/')
@login_required
def show_chat(id):
    chat = session.query(Chat).filter_by(id=id).first()
    messages = session.query(Message, User).filter_by(chat_id=id).order_by(Message.time.desc()).join(User).limit(50)
    return render_template('chat_channel.html', messages = list(reversed(list(messages))), chat = chat)

@socketio.on('message')
def handleMessage(msg):
    chat = msg[-1]
    usr = msg[-3:-2]
    msg = msg[0:-4]
    message = Message(msg, usr, chat)
    if '/stock=' in msg:
        message_to_send = (get_quote_per_share(msg))
        send(message_to_send, broadcast=True)

    else:
        msg = current_user.username + ": " + msg
        session.add(message)
        session.commit()

        send(msg, broadcast=True)

@app.route('/create_channel/', methods=["GET","POST"])
@login_required
def create_channel():
    if request.method == 'POST':
        name = request.form['name']
        name = name.strip()
        qry_name = session.query(Chat).filter_by(name=name).first()
        if qry_name:
            print("Channel already exists")
            
        else:
            new_chat = Chat(name)
            session.add(new_chat)
            session.commit()
        return render_template('index.html', user = current_user)
    elif request.method == 'GET' and current_user.admin == '1':
        return render_template('create_channel.html')
    elif request.method == 'GET' and current_user.admin == '0':
        return render_template('index.html', user = current_user)

if __name__ == '__main__':
    # TODO: Remove debug=True before sending the challenge
    app.run(host="0.0.0.0")