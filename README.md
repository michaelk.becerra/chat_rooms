# Financial Chat

Financial Chat is a web app built in python that get stocks from API.

## Installation

After cloning the repository, install dependencies

```bash
pip install -r requirements.txt
```

## Usage

```bash
python3 web.py 
```

